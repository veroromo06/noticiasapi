import { Injectable } from '@angular/core';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NoticiasService {

  private url = 'https://newsapi.org/v2/everything?domains=wsj.com&apiKey=158c40fca03b40ddb3bf7243f41a0c0c';

  constructor(private http: HttpClient) { }

  public getNoticias(): Observable<any>{
    return this.http.get(this.url);
  }

  
}
