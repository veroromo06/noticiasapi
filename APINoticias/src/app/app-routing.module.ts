import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NoticiaComponent } from './Components/noticia/noticia/noticia.component';
import { NoticiasComponent } from './Components/noticias/noticias/noticias.component';

const routes: Routes = [
  {path: 'noticia', component: NoticiaComponent},
  {path: 'noticias', component: NoticiasComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
