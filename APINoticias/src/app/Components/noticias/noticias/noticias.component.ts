import { Component, OnInit, Input } from '@angular/core';
import { NoticiasService } from 'src/app/Services/noticias.service';

@Component({
  selector: 'app-noticias',
  templateUrl: './noticias.component.html',
  styleUrls: ['./noticias.component.css']
})
export class NoticiasComponent implements OnInit {
  @Input() noti: any;
  noticias: any = [];

  constructor(public noticiasService:NoticiasService) { }

  ngOnInit(): void {
    this.noticias = this.noticiasService.getNoticias().subscribe(data=>{
      this.noticias = data.articles;
      console.log(data.articles);
    });
    
  }

  public noticiac(indice){
    let noti = this.noticias[indice];
    console.log(indice);
    console.log(noti);
  }

}
