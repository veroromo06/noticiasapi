import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-noticia',
  templateUrl: './noticia.component.html',
  styleUrls: ['./noticia.component.css']
})
export class NoticiaComponent implements OnInit {
  
  @Input() noti: any;
  
  constructor() { }

  ngOnInit(): void {
  }

  public noticiac(indice){
    console.log(indice);
  }

}
